package com.ec.item;

import lombok.Data;

@Data
public class tmpBeans extends ItemBeans{
	private static final long serialVersionUID = 1L;
	private int tmpCnt;
	public tmpBeans(int item_id, String item_name, int price, String detail, int inventory, String img_name ,int count) {
		super(item_id, item_name, price, detail, inventory, img_name);
		tmpCnt = count;
	}
	public void increment() {
		this.tmpCnt++; 
	}
}
