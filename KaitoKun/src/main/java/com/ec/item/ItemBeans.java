package com.ec.item;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ItemBeans {
	protected static final long serialVersionUID = 1L;
	protected int item_id;
	protected String item_name;
	protected int price;
	protected String detail;
	protected int inventory;
	protected String img_name;
}