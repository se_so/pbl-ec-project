package com.ec.item;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class OrderBeans {
	private static final long serialVersionUID = 1L;
	private int order_id;
	private String img_name;
	private String item_name;
	private Date order_date;
	private int quantity_ordered;

}
