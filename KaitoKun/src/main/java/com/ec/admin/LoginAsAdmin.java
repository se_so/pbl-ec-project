package com.ec.admin;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ec.util.ConToDB;

@WebServlet("/LoginAsAdmin")
public class LoginAsAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private final String searchAccountSQL = "SELECT * FROM admins WHERE admin_id = ? AND password = ?";
	private final String updateDateSQL = "UPDATE admins SET last_login = ? WHERE admin_id = ?";

	private String admin_id,password;
	private Date tmp;
	private ResultSet rs;
	private AdminBeans admin, resultOfSearch;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");

		admin_id = req.getParameter("admin_id");
		password = req.getParameter("password");

		try {
			Connection con = ConToDB.getConnection();
			resultOfSearch = searchUser(admin_id, password, con);
			if (resultOfSearch == null) {
				req.setAttribute("result", "Administrator login is not allowed.");
				req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
			} else {
				HttpSession sessionPost = req.getSession();
				sessionPost.setAttribute("admin_info", resultOfSearch);
				insert(resultOfSearch.getAdmin_id(), con);
				req.getServletContext().getRequestDispatcher("/WEB-INF/admin_jsp/manage.jsp").forward(req, res);
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");
		req.getServletContext().getRequestDispatcher("/WEB-INF/admin_jsp/admin_login.jsp").forward(req, res);

	}

	private AdminBeans searchUser(String id, String pass, Connection con) {
		try (PreparedStatement ps = con.prepareStatement(searchAccountSQL)) {
			int adminId = 0;
			if (id != "") {
				adminId = Integer.parseInt(id);
			}
			ps.setInt(1, adminId);
			ps.setString(2, pass);
			rs = ps.executeQuery();
			while (rs.next()) {
				admin = new AdminBeans(rs.getInt("admin_id"), rs.getString("password"), rs.getDate("last_login"));
			}
			return admin;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private void insert(int admin_id, Connection con) {
		tmp = new Date();
		java.sql.Date date = new java.sql.Date(tmp.getTime());
		try (PreparedStatement ps = con.prepareStatement(updateDateSQL)) {
			ps.setDate(1, date);
			ps.setInt(2, admin_id);
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}