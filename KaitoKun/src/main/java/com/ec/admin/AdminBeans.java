package com.ec.admin;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class AdminBeans {
	private static final long serialVersionUID = 1L;
	private int admin_id;
	private String password;
	private Date last_login;
}
