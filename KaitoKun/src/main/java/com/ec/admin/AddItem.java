package com.ec.admin;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.ec.util.ConToDB;

@WebServlet("/AddItem")
@MultipartConfig(location = "/tmp", maxFileSize = 1048576)
public class AddItem extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String insertSQL = "INSERT INTO items (item_name,price,detail,inventory,img_name,category_id) VALUES (?,?,?,?,?,?)";
	private String name, item_name, item_price, item_detail, inventory, category_id;
	private Part part;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");

		try {
			item_name = req.getParameter("name");
			item_price = req.getParameter("price");
			item_detail = req.getParameter("detail");
			inventory = req.getParameter("inventory");
			category_id = req.getParameter("category_id");

			part = req.getPart("file");
			name = this.getFileName(part);
			part.write("/Users/so/git/pbl-ec-project/KaitoKun/src/main/webapp/img/" + name);

			Connection con = ConToDB.getConnection();
			if (!item_name.equals("") && !item_price.equals("") && !item_detail.equals("") && !inventory.equals("")
					&& !name.equals("") && !category_id.equals("")) {
				addItem(item_name, item_price, item_detail, inventory, name, category_id, con);
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		req.getServletContext().getRequestDispatcher("/WEB-INF/admin_jsp/manage.jsp").forward(req, res);

	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");
		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
	}

	private String getFileName(Part part) {
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}

	private void addItem(String item_name, String price, String detail, String inventory, String img_name,
			String category_id,
			Connection con) {
		try (PreparedStatement ps = con.prepareStatement(insertSQL)) {
			int p = Integer.parseInt(price);
			int inv = Integer.parseInt(inventory);
			int cId = Integer.parseInt(category_id);
			ps.setString(1, item_name);
			ps.setInt(2, p);
			ps.setString(3, detail);
			ps.setInt(4, inv);
			ps.setString(5, img_name);
			ps.setInt(6, cId);
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}