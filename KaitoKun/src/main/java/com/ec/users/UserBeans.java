package com.ec.users;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class UserBeans {
	private static final long serialVersionUID = 1L;
	private int user_id;
	private String user_name;
	private String password;
	private String phone_num;
	private String address;
	private Date last_login;
	private String e_mail;
}
