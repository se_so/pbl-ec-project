package com.ec.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/Logout")
public class Logout extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");
		
		HttpSession session = req.getSession(false);

		if (session != null) {
			session.invalidate();
		}
		
		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");

		HttpSession session = req.getSession(false);

		if (session != null) {
			session.invalidate();
		}

		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
	}
}