package com.ec.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ec.users.UserBeans;
import com.ec.util.ConToDB;

@WebServlet("/AddInfo")
public class AddInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String updateSQL = "UPDATE users SET user_name = ?,phone_num = ?, address = ? WHERE user_id = ?";
	private final String searchAccountSQL = "SELECT * FROM users WHERE e_mail = ? AND password = ?";

	private String name, phone_num, address, password, e_mail;

	private UserBeans user, resultOfSearch;

	private ResultSet rs;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");

		HttpSession session = req.getSession(false);
		if (session == null) {
			req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
			return;
		}
		name = req.getParameter("name");
		phone_num = req.getParameter("phone_num");
		address = req.getParameter("address");
		e_mail = ((UserBeans) session.getAttribute("user_info")).getE_mail();
		password = ((UserBeans) session.getAttribute("user_info")).getPassword();

		try {
			Connection con = ConToDB.getConnection();
			insert(name, phone_num, address, session, con);
			resultOfSearch = searchUser(e_mail, password, con);
			session.setAttribute("user_info", resultOfSearch);
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/main.jsp").forward(req, res);
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");
		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
	}

	private void insert(String name, String phone_num, String address, HttpSession session, Connection con) {
		if (phone_num != null && address != null && phone_num != "" && address != "" && name != null && name != "") {
			int user_id = ((UserBeans) session.getAttribute("user_info")).getUser_id();
			try (PreparedStatement ps = con.prepareStatement(updateSQL)) {
				ps.setString(1, name);
				ps.setString(2, phone_num);
				ps.setString(3, address);
				ps.setInt(4, user_id);
				ps.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private UserBeans searchUser(String e_mail, String password, Connection con) {
		try (PreparedStatement ps = con.prepareStatement(searchAccountSQL)) {
			ps.setString(1, e_mail);
			ps.setString(2, password);
			rs = ps.executeQuery();
			while (rs.next()) {
				user = new UserBeans(rs.getInt("user_id"), rs.getString("user_name"),
						rs.getString("password"),
						rs.getString("phone_num"), rs.getString("address"), rs.getDate("last_login"),
						rs.getString("e_mail"));
			}
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}