package com.ec.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ec.item.ItemBeans;
import com.ec.users.UserBeans;
import com.ec.util.ConToDB;

@WebServlet("/Pole")
public class Pole extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private final String searchAccountSQL = "SELECT * FROM users WHERE e_mail = ? AND password = ?";
	private final String updateDateSQL = "UPDATE users SET  last_login = ? WHERE user_id = ?";
	private final String searchAllItemSQL = "SELECT * FROM items";
	private String e_mail;
	private String password;
	private UserBeans resultOfSearch, authorized_user;
	private Date tmp;
	private ArrayList<ItemBeans> list;

	private ResultSet rs;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");

		e_mail = req.getParameter("e-mail");
		password = req.getParameter("password");

		try {
			Connection con = ConToDB.getConnection();
			resultOfSearch = searchUser(e_mail, password, con);

			//未入力または該当アカウントなしで再度LoginPageへ
			if (resultOfSearch == null || e_mail.equals("") || password.equals("")) {
				req.setAttribute("result", "Password or e-mail address is incorrect. Please re-enter.");
				req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
			} else {
				HttpSession sessionPost = req.getSession();
				sessionPost.setAttribute("user_info", resultOfSearch);
				sessionPost.setAttribute("items", searchAllItem(con));
				sessionPost.setAttribute("cart", new ArrayList<ItemBeans>());
				insert_date_log(resultOfSearch.getUser_id(), con);
				
				//初回Loginで住所と電話番号を登録
				if (resultOfSearch.getAddress() == null || resultOfSearch.getPhone_num() == null) {
					req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/register.jsp").forward(req, res);
				} else {
					req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/main.jsp").forward(req, res);
				}
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");

		e_mail = req.getParameter("e-mail");
		password = req.getParameter("password");

		try {
			Connection con = ConToDB.getConnection();
			resultOfSearch = searchUser(e_mail, password, con);
			if (resultOfSearch == null || e_mail.equals("") || password.equals("")) {
				req.setAttribute("result", "Password or e-mail address is incorrect. Please re-enter.");
				req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
			} else {
				HttpSession sessionPost = req.getSession();
				sessionPost.setAttribute("user_info", resultOfSearch);
				sessionPost.setAttribute("items", searchAllItem(con));
				sessionPost.setAttribute("cart", new ArrayList<ItemBeans>());
				insert_date_log(resultOfSearch.getUser_id(), con);
				if (resultOfSearch.getAddress() == null || resultOfSearch.getPhone_num() == null) {
					req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/register.jsp").forward(req, res);
				} else {
					req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/main.jsp").forward(req, res);
				}
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private UserBeans searchUser(String e_mail, String password, Connection con) {
		if (e_mail != null && password != null && !e_mail.equals("") && !password.equals("")) {
			try (PreparedStatement ps = con.prepareStatement(searchAccountSQL)) {
				ps.setString(1, e_mail);
				ps.setString(2, password);
				rs = ps.executeQuery();
				while (rs.next()) {
					authorized_user = new UserBeans(rs.getInt("user_id"), rs.getString("user_name"),
							rs.getString("password"),
							rs.getString("phone_num"), rs.getString("address"), rs.getDate("last_login"),
							rs.getString("e_mail"));
				}
				return authorized_user;
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				try {
					if (rs != null) {
						rs.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		} else {
			return null;
		}

	}

	private void insert_date_log(int user_id, Connection con) {
		tmp = new Date();
		java.sql.Date date = new java.sql.Date(tmp.getTime());
		try (PreparedStatement ps = con.prepareStatement(updateDateSQL)) {
			ps.setDate(1, date);
			ps.setInt(2, user_id);
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private ArrayList<ItemBeans> searchAllItem(Connection con) {
		try (Statement st = con.createStatement(); ResultSet rs = st.executeQuery(searchAllItemSQL)) {
			list = new ArrayList<>();
			while (rs.next()) {
				if (rs.getInt("inventory") > 0) {
					list.add(new ItemBeans(rs.getInt("item_id"), rs.getString("item_name"), rs.getInt("price"),
							rs.getString("detail"), rs.getInt("inventory"), rs.getString("img_name")));
				}
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}