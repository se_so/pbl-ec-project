package com.ec.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ec.item.ItemBeans;

@WebServlet("/Move")
public class Move extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ItemBeans item;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");

		HttpSession session = req.getSession(false);
		if (session == null) {
			req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
			return;
		}

		item = getSelectedItem(session.getAttribute("items"), req.getParameter("item_id"));

		req.setAttribute("selected_item", item);
		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/item.jsp").forward(req, res);
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");
		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/main.jsp").forward(req, res);
	}

	private ItemBeans getSelectedItem(Object items, String item_id) {
		ItemBeans tmpItem = null;
		for (ItemBeans item : (ArrayList<ItemBeans>) items) {
			if (item.getItem_id() == Integer.parseInt(item_id)) {
				tmpItem = item;
				break;
			}
		}
		return tmpItem;
	}

}