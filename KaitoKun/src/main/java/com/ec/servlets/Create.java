package com.ec.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ec.users.UserBeans;
import com.ec.util.ConToDB;

@WebServlet("/Create")
public class Create extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private final String insertSQL = "INSERT INTO users (password,e_mail,last_login) VALUES (?,?,?)";
	private final String searchAccountSQL = "SELECT * FROM users WHERE e_mail = ?";
	private String e_mail;
	private String password;
	private Date tmp;
	private UserBeans user;

	private ResultSet rs;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");

		e_mail = req.getParameter("e-mail");
		password = req.getParameter("password");

		try {
			Connection con = ConToDB.getConnection();
			if (searchUser(e_mail, con) == null) {
				insert_date_login(password, e_mail, con);
			} else {
				req.setAttribute("result", "An account with that email addres already exists.");
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");
		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/create.jsp").forward(req, res);
	}

	private void insert_date_login(String password, String e_mail, Connection con) {
		tmp = new Date();
		java.sql.Date date = new java.sql.Date(tmp.getTime());
		try (PreparedStatement ps = con.prepareStatement(insertSQL)) {
			ps.setString(1, password);
			ps.setString(2, e_mail);
			ps.setDate(3, date);
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private UserBeans searchUser(String e_mail, Connection con) {
		try (PreparedStatement ps = con.prepareStatement(searchAccountSQL)) {
			ps.setString(1, e_mail);
			rs = ps.executeQuery();
			while (rs.next()) {
				user = new UserBeans(rs.getInt("user_id"), rs.getString("user_name"),
						rs.getString("password"),
						rs.getString("phone_num"), rs.getString("address"), rs.getDate("last_login"),rs.getString("e_mail"));
			}
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}