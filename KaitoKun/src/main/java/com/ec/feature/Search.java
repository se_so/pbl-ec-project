package com.ec.feature;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ec.item.ItemBeans;
import com.ec.util.ConToDB;

@WebServlet("/Search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String searchItemSQL = "SELECT * FROM items WHERE item_name LIKE ?";
	private final String searchAllItemSQL = "SELECT * FROM items";
	private final String searchAllOrderByItemSQL = "SELECT * FROM items ORDER BY price";

	ArrayList<ItemBeans> list, target;

	private String item_name;
	private ResultSet rs;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");
		HttpSession session = req.getSession(false);
		if (session == null) {
			req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
			return;
		}
		try {
			Connection con = ConToDB.getConnection();
			item_name = req.getParameter("hint");
			target = searchItem(item_name, con);
			session.setAttribute("items", target);
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/main.jsp").forward(req, res);
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");
		HttpSession session = req.getSession(false);
		try {
			Connection con = ConToDB.getConnection();
			if (req.getParameter("flag").equals("all")) {
				target = searchAllItem(con);
			} else if (req.getParameter("flag").equals("order")) {
				target = searchAllOrderByItem(con);
			}
			session.setAttribute("items", target);
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/main.jsp").forward(req, res);

	}

	private ArrayList<ItemBeans> searchItem(String item_name, Connection con) {
		try (PreparedStatement ps = con.prepareStatement(searchItemSQL)) {
			ps.setString(1, "%" + item_name + "%");
			rs = ps.executeQuery();
			list = new ArrayList<>();
			while (rs.next()) {
				list.add(new ItemBeans(rs.getInt("item_id"), rs.getString("item_name"), rs.getInt("price"),
						rs.getString("detail"), rs.getInt("inventory"), rs.getString("img_name")));
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	private ArrayList<ItemBeans> searchAllItem(Connection con) {
		try (Statement st = con.createStatement(); ResultSet rs = st.executeQuery(searchAllItemSQL)) {
			list = new ArrayList<>();
			while (rs.next()) {
				if (rs.getInt("inventory") > 0) {
					list.add(new ItemBeans(rs.getInt("item_id"), rs.getString("item_name"), rs.getInt("price"),
							rs.getString("detail"), rs.getInt("inventory"), rs.getString("img_name")));
				}
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	private ArrayList<ItemBeans> searchAllOrderByItem(Connection con) {
		try (Statement st = con.createStatement(); ResultSet rs = st.executeQuery(searchAllOrderByItemSQL)) {
			list = new ArrayList<>();
			while (rs.next()) {
				if (rs.getInt("inventory") > 0) {
					list.add(new ItemBeans(rs.getInt("item_id"), rs.getString("item_name"), rs.getInt("price"),
							rs.getString("detail"), rs.getInt("inventory"), rs.getString("img_name")));
				}
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
