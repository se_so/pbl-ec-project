package com.ec.feature;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ec.item.tmpBeans;

@WebServlet("/RemoveFromCart")
public class RemoveFromCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String remove_id;
	private ArrayList<tmpBeans> cart;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");
		HttpSession session = req.getSession(false);
		if (session == null) {
			req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
			return;
		}
		remove_id = req.getParameter("item_id");
		cart = remove(session.getAttribute("cart"), remove_id);
		session.setAttribute("cart", cart);
		session.setAttribute("order_sum_price", sum(cart));
		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/main.jsp").forward(req, res);
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");
	}

	private ArrayList<tmpBeans> remove(Object obj, String id) {
		int item_id = Integer.parseInt(id);
		ArrayList<tmpBeans> list = (ArrayList<tmpBeans>) obj;
		for (tmpBeans item : list) {
			if (item.getItem_id() == item_id) {
				list.remove(list.indexOf(item));
				break;
			}
		}
		return list;
	}

	private int sum(ArrayList<tmpBeans> list) {
		int sum = 0;
		for (tmpBeans item : list) {
			sum += item.getPrice() * item.getTmpCnt();
		}
		return sum;
	}
}