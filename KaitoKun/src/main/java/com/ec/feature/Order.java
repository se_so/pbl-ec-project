package com.ec.feature;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ec.item.OrderBeans;
import com.ec.users.UserBeans;
import com.ec.util.ConToDB;

@WebServlet("/Order")
public class Order extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String searchOrderListSQL = "SELECT* FROM order_list WHERE user_id = ?";
	private String searchImgSQL = "SELECT img_name,item_name FROM items WHERE item_id = ?";

	private ResultSet rs1, rs2;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");

		HttpSession session = req.getSession(false);
		if (session == null) {
			req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
			return;
		}
		try {
			Connection con = ConToDB.getConnection();
			int id = ((UserBeans) session.getAttribute("user_info")).getUser_id();
			session.setAttribute("order_list", searchOrderList(id, con));
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/order.jsp").forward(req, res);
	}

	private ArrayList<OrderBeans> searchOrderList(int user_id, Connection con) {
		ArrayList<OrderBeans> list = new ArrayList<>();
		try (PreparedStatement ps1 = con.prepareStatement(searchOrderListSQL);
				PreparedStatement ps2 = con.prepareStatement(searchImgSQL)) {
			ps1.setInt(1, user_id);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				ps2.setInt(1, rs1.getInt("item_id"));
				rs2 = ps2.executeQuery();
				rs2.next();
				list.add(new OrderBeans(rs1.getInt("order_id"), rs2.getString("img_name"), rs2.getString("item_name"),
						rs1.getDate("order_date"),
						rs1.getInt("quantity_ordered")));
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (rs1 != null)
					rs1.close();
				if (rs2 != null)
					rs2.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}