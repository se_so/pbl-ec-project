package com.ec.feature;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ec.item.tmpBeans;
import com.ec.util.ConToDB;

@WebServlet("/AddToCart")
public class AddToCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String searchItemSQL = "SELECT* FROM items WHERE item_id = ?";

	private ResultSet rs;
	private tmpBeans tmp;
	private ArrayList<tmpBeans> cart, cartTmp;
	private int sum;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");

		HttpSession session = req.getSession(false);
		if (session == null) {
			req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
			return;
		}
		try {
			Connection con = ConToDB.getConnection();
			cart = (ArrayList<tmpBeans>) session.getAttribute("cart");
			//カート内に同じアイテムがあれば、個数を1増やして更新する
			cartTmp = sort(cart, searchItem(req.getParameter("item"), con));
			session.setAttribute("cart", cartTmp);
			session.setAttribute("order_sum_price", sum(cartTmp));
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/main.jsp").forward(req, res);
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");
	}

	private tmpBeans searchItem(String item_id, Connection con) {
		try (PreparedStatement ps = con.prepareStatement(searchItemSQL)) {
			ps.setInt(1, Integer.parseInt(item_id));
			rs = ps.executeQuery();
			while (rs.next()) {
				tmp = new tmpBeans(rs.getInt("item_id"), rs.getString("item_name"), rs.getInt("price"),
						rs.getString("detail"), rs.getInt("inventory"), rs.getString("img_name"), 1);
			}
			return tmp;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public ArrayList<tmpBeans> sort(ArrayList<tmpBeans> list, tmpBeans tmpbean) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getItem_id() == tmpbean.getItem_id()) {
				list.get(i).increment();
				return list;
			}
		}
		list.add(tmpbean);
		return list;
	}

	private int sum(ArrayList<tmpBeans> list) {
		sum = 0;
		for (tmpBeans item : list) {
			sum += item.getPrice() * item.getTmpCnt();
		}
		return sum;
	}
}