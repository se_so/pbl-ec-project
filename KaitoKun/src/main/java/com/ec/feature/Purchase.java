package com.ec.feature;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ec.item.tmpBeans;
import com.ec.users.UserBeans;
import com.ec.util.ConToDB;

@WebServlet("/Purchase")
public class Purchase extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private final String insertSQL = "INSERT INTO order_list (user_id,item_id,order_date,quantity_ordered) VALUES (?,?,?,?)";
	private final String updateSQL = "UPDATE items SET inventory = ? WHERE item_id = ?";
	private final String selectSQL = "SELECT inventory FROM items WHERE item_id = ?";

	private ResultSet rs;
	private UserBeans user;
	private ArrayList<tmpBeans> order_list;
	private Date tmp;
	private java.sql.Date date;
	

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");

		HttpSession session = req.getSession(false);
		if (session == null) {
			req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, res);
			return;
		}
		user = (UserBeans) session.getAttribute("user_info");
		order_list = (ArrayList<tmpBeans>) session.getAttribute("cart");
		try {
			tmp = new Date();
			date = new java.sql.Date(tmp.getTime());
			Connection con = ConToDB.getConnection();
			//在庫が足らなない場合は購入を中止する、メッセージを表示する
			if (update_item_inventory(order_list, con)) {
				insert_order_log(user.getUser_id(), date, order_list, con);
				session.setAttribute("cart", new ArrayList<tmpBeans>());
				req.setAttribute("flag", "The purchase has been completed.Thank you very much.");
			} else {
				req.setAttribute("flag", "The purchase failed for some reason.");
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		req.getServletContext().getRequestDispatcher("/WEB-INF/jsp/main.jsp").forward(req, res);
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		req.setCharacterEncoding("UTF-8");

	}

	private void insert_order_log(int user_id, java.sql.Date date, ArrayList<tmpBeans> list, Connection con) {
		for (tmpBeans tmp : list) {
			try (PreparedStatement ps = con.prepareStatement(insertSQL)) {
				ps.setInt(1, user_id);
				ps.setInt(2, tmp.getItem_id());
				ps.setDate(3, date);
				ps.setInt(4, tmp.getTmpCnt());
				ps.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private boolean update_item_inventory(ArrayList<tmpBeans> list, Connection con) {
		boolean flag = true;
		for (tmpBeans tmp : list) {
			try (PreparedStatement ps1 = con.prepareStatement(updateSQL);
					PreparedStatement ps2 = con.prepareStatement(selectSQL);) {
				ps2.setInt(1, tmp.getItem_id());
				rs = ps2.executeQuery();
				while (rs.next()) {
					int store = rs.getInt("inventory") - tmp.getTmpCnt();
					if (store < 0) {
						flag = false;
						break;
					}
					ps1.setInt(1, store);
					ps1.setInt(2, tmp.getItem_id());
					ps1.execute();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return flag;
	}
}