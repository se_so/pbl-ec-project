package com.ec.util;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConToDB implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Context initContext,envContext;
	private static DataSource ds;

	public static Connection getConnection() {
		try {
			initContext = new InitialContext();
			envContext = (Context) initContext.lookup("java:/comp/env");
			ds = (DataSource) envContext.lookup("jdbc/mysql");
			return ds.getConnection();

		} catch (NamingException | SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
