<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>KaitoKuuuuN Admin</title>
</head>
<body>
	<div class="container py-5 px-5 h-100 w-50">
		<div class="row d-flex justify-content-center align-items-start h-100">
			<div class="col">
				<div class="card"
					style="border-radius: .75rem; background-color: #eff1f2;">
					<div class="card-body py-4 px-4 px-md-5">
						<h1 class="text-center fst-italic pb-3">Add Item Page</h1>
						<hr class="mt-1 mb-4">
						<form method="POST" action="AddItem" enctype="multipart/form-data"
							class="d-flex flex-column align-items-start mb-4">
							<p class="fs-6 fst-italic mb-0">select upload file</p>
							<input type="file" name="file" required class="w-75 mb-4" />
							<p class="fs-6 fst-italic mb-0">item name</p>
							<input type="text" placeholder="ENTER ITEM NAME" name="name"
								class="w-50 mb-4" />
							<div class="w-50">
								<p class="fs-6 fst-italic mb-0">item price</p>
								<input type="text" placeholder="ENTER ITEM PRICE" name="price"
									class="w-75 mb-4" />
								<p class="fs-6 fst-italic mb-0">item inventory</p>
								<input type="text" placeholder="ENTER ITEM INVENTORY"
									name="inventory" class="w-75 mb-4" />
							</div>
							<p class="fs-6 fst-italic mb-0">item detail</p>
							<div class="input-group w-75 mb-1">
								<textarea placeholder="ENTER ITEM DETAIL" class="form-control"
									name=detail aria-label="With textarea"></textarea>
							</div>
							<label for="category_id" class="fs-7 fst-italic mb-0">Category
								ID:</label> <select name="category_id" id="category_id"
								class="mb-4 fs-7 fst-italic">
								<option value="1">1 - Household items</option>
								<option value="2">2 - Fashion</option>
								<option value="3">3 - Miscellaneous items</option>
								<option value="4">4 - Others</option>
							</select>
							<button type="submit" class="btn btn-outline-secondary px-5 ms-5">ADD</button>
						</form>
						<div class="my-2 text-center">
							<a href="Logout" style="text-decoration: none; color: gray;">back</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>