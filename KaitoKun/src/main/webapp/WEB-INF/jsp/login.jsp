<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>Login Page</title>
</head>
<body>
	<header class="container-fluid  bg-primary"> </header>

	<div class="container py-5 px-5 h-100 w-50">
		<div
			class="row d-flex justify-content-center align-items-center h-100">
			<div class="col">
				<div class="card"
					style="border-radius: .75rem; background-color: #eff1f2;">
					<div class="card-body py-4 px-4 px-md-5">
						<h1 class="text-center text-secondary fst-italic pb-3">Login Page</h1>
						<hr class="mt-1 mb-4">
						<form method="POST" action="Pole"
							class="d-flex flex-column align-items-center mb-4">
							<input type="text" placeholder="E-Mail" name="e-mail"
								class="w-50 mb-2" /> <input type="text"
								placeholder="ENTER PASSWORD" name="password"
								class="w-50 mb-2 mt-2" />
							<button type="submit" class="btn btn-outline-secondary px-5 mt-4">Login</button>
						</form>
						<hr class="mt-1 mb-4">
						<div class="my-2 text-center">
							<a href="Create"  class="text-secondary"style="text-decoration: none;">create
								new account</a>
						</div>
						<c:if test="${!empty(result)}">
							<div>
								<p class="text-center" style="color: red;">
									<c:out value="${result}"></c:out>
								</p>
							</div>
						</c:if>
						<div class="my-2 text-center">
							<a href="LoginAsAdmin"
								style="text-decoration: none; color: gray;">login as admin</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>