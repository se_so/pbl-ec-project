<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>Order List Page</title>
</head>
<body>
	<div class="d-flex flex-column align-items-center pt-5">
		<div class="w-100">
			<p class="fst-italic fs-1 text-center">KaitoKuuuuN</p>
			<hr class="mt-1 mb-4">
		</div>
		<div class="container pt-5 px-5 h-70 w-70">
			<div
				class="row d-flex justify-content-center align-items-center h-70 w-100">
				<c:if test="${!empty(selected_item)}">
					<div class="card d-flex flex-row p-0"
						style="width: 50%;">
						<div class="w-50">
							<img src="img/${selected_item.getImg_name()}"
								class="card-img-top" alt="item" style="">
						</div>
						<div
							class="card-body d-flex flex-column justify-content-around py-0 w-50">
							<div>
								<h5 class="card-title m-0">
									<c:out value="${selected_item.getItem_name()}"></c:out>
								</h5>
								<p class="card-text">
									<c:out value="¥${selected_item.getPrice()}"></c:out>
								</p>
							</div>
							<p class="card-text " style="overflow-wrap: break-word;">
								<c:out value="${selected_item.getDetail()}"></c:out>
							</p>
							<div>
								<form method="POST" action="AddToCart" class="my-2 text-end">
									<input type="hidden" name="item"
										value="${selected_item.getItem_id()}">
									<button type="submit" class="btn btn-outline-dark">Add
										to cart</button>
								</form>
							</div>
						</div>
					</div>
				</c:if>
				<div class="my-5 text-center">
					<a href="Move" style="text-decoration: none; color: gray;">Back</a>
				</div>
			</div>
		</div>
	</div>

</body>
</html>