<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>Register Page</title>
</head>
<body>
	<header class="container-fluid  bg-primary">
		<div class="d-flex flex-row-reverse">
			<div class="col-1 my-2 text-center">
				<a href="#"></a>
			</div>
			<div class="col-1 my-2 text-center">
				<a href="#"></a>
			</div>
		</div>
	</header>
<body>
	<div class="container py-5 px-5 h-100 w-50">
		<div
			class="row d-flex justify-content-center align-items-center h-100">
			<div class="col">
				<div class="card"
					style="border-radius: .75rem; background-color: #eff1f2;">
					<div class="card-body py-4 px-4 px-md-5">
						<h1 class="text-center text-primary fst-italic pb-3">First
							Register Page</h1>
						<hr class="mt-1 mb-4">
						<form method="POST" action="AddInfo"
							class="d-flex flex-column align-items-center mb-4">
							<p class="fs-5 fst-italic mb-0">what's your nick name ?</p>
							<input type="text" placeholder="ENTER NICK NAME" name="name"
								class="w-50 mb-3" />
							<p class="fs-5 fst-italic mb-0">your address</p>
							<input type="text" placeholder="ENTER ADDRESS" name="address"
								class="w-50 mb-3" />
							<p class="fs-5 fst-italic mb-0">your phone number</p>
							<input type="text" placeholder="ENTER PHONE NUMBER"
								name="phone_num" class="w-50 mb-3" />
							<button type="submit" class="btn btn-outline-secondary px-5">Register</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>