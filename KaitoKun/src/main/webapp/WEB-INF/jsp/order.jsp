<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>Order List Page</title>
</head>
<body>
	<div class="d-flex flex-column align-items-center pt-5">
		<div class="w-100">
			<p class="fst-italic fs-1 text-center">Order List</p>
			<hr class="mt-1 mb-4">
		</div>
		<div class="container pt-5 px-5 h-70 w-70">
			<div
				class="d-flex flex-column justify-content-center align-items-center h-70 w-100">
				<c:if test="${!empty(order_list)}">
					<c:forEach var="item" items="${order_list}">
						<div class="card d-flex flex-row p-0 mb-3" style="width: 50%;">
							<div class="w-50">
								<img src="img/${item.getImg_name()}" class="card-img-top"
									alt="item" style="">
							</div>
							<div
								class="card-body d-flex flex-column py-0 w-50">

								<h5 class="card-title mt-3">
									<c:out value="${item.getItem_name()}"></c:out>
								</h5>
								<hr class="my-0">
								<p class="card-text mt-2 mb-0">
									<c:out value="${item.getOrder_date()}"></c:out>
								</p>
								<p class="card-text ">
									<c:out value="Quantity : ${item.getQuantity_ordered()}"></c:out>
								</p>

							</div>
						</div>
					</c:forEach>
				</c:if>
				<div class="my-5 text-center">
					<a href="Move" style="text-decoration: none; color: gray;">Back</a>
				</div>
			</div>
		</div>
	</div>

</body>
</html>