<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>KaitoKuuuuN</title>
<style type="text/css">
.btn-circle.btn-xl {
	width: 20px;
	height: 20px;
	padding: 2px 3px 3px 3px;
	border-radius: 60px;
	font-size: 5px;
	text-align: center;
}

.dropdown-menu {
	position: absolute;
	z-index: 1000;
	display: none;
	min-width: 300px;
	padding: 0.5rem 0;
	margin: 0;
	font-size: 1rem;
	color: #212529;
	text-align: left;
	list-style: none;
	background-color: #fff;
	background-clip: padding-box;
	border: 1px solid rgba(0, 0, 0, .15);
	border-radius: 0.25rem;
}
</style>

</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container-fluid mb-3">
			<div class='fixed-top bg-light d-flex flex-row align-items-center'
				style="height: 80px;">
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ms-5 me-auto mb-2 mb-lg-0">
						<li class="nav-item dropdown"><a
							class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
							role="button" data-bs-toggle="dropdown" aria-expanded="false">
								User Info </a> <c:if test="${!empty(user_info)}">
								<ul class="dropdown-menu fst-italic px-2 m-3 w-100"
									aria-labelledby="navbarDropdown">
									<li>ID : <c:out value="${user_info.getUser_id()}"></c:out></li>
									<hr class="mt-1 mb-1">
									<li>Name : <c:out value="${user_info.getUser_name()}"></c:out></li>
									<hr class="mt-1 mb-1">
									<li>
										<div class="fs-6">
											<c:out value="Email : ${user_info.getE_mail()}"></c:out>
										</div>
									</li>
									<hr class="mt-1 mb-1">
									<li><c:out
											value="Last Login : ${user_info.getLast_login()}"></c:out></li>
									<hr class="mt-1 mb-1">
									<li><a href="Order"
										style="text-decoration: none; color: black;">Order History</a>
								</ul>
							</c:if></li>
					</ul>
					<div
						class="d-flex flex-row align-items-center w-50 justify-content-end">
						<form class="d-flex w-75 h-50" method="POST" action="Search">
							<input class="form-control me-2 w-75" type="search"
								placeholder="Search" aria-label="Search" name="hint">
							<button class="btn btn-outline-secondary w-25" type="submit">Search</button>
						</form>
						<form method="POST" action="Logout" class="me-5 ms-2">
							<a class="nav-link" href="Logout" tabindex="-1"
								aria-disabled="true" style="text-decoration: none; color: gray;">Logout</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</nav>


	<div class="container mt-5 pt-5">
		<div>
			<p class="fst-italic fs-1 text-center">KaitoKuuuuN</p>
			<hr class="mt-1 mb-4">
		</div>
		<div
			class="d-flex flex-row justify-content-between align-items-center">
			<div class="d-flex flex-row">
				<form mathod="GET" action="Search" class="ms-5">
					<input type="hidden" name="flag" value="all" />
					<button type="submit" class="btn btn-outline-dark">Show
						All Item</button>
				</form>
				<form mathod="GET" action="Search" class="ms-5">
					<input type="hidden" name="flag" value="order" />
					<button type="submit" class="btn btn-outline-dark">Price
						Order</button>
				</form>
			</div>
			<div class="text-end">
				<a type="button" data-bs-toggle="modal" data-bs-target="#Modal"
					class="me-4" style="text-decoration: none; color: black;"><img
					src="img/cart.png" alt="cart" style="width: 20%; height: 20%;"
					class="me-4">
					<p class="me-4">show cart</p></a>
			</div>
		</div>

		<c:if test="${!empty(flag)}">
			<div class="text-center mb-3 fs-4 fst-italic text-primary">
				<c:out value="${flag}"></c:out>
			</div>
		</c:if>
		<div class="modal fade" id="Modal" tabindex="-1"
			aria-labelledby="ModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="ModalLabel">Order List</h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal"
							aria-label="Close"></button>
					</div>
					<c:if test="${empty(cart)}">
						<div class="modal-body">
							<h5>Nothing in cart</h5>
							<p>Enjoy your shopping!!</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-outline-secondary"
								data-bs-dismiss="modal">Close</button>
						</div>
					</c:if>
					<c:if test="${!empty(cart)}">
						<div class="modal-body">
							<c:forEach var="item" items="${cart}">
								<div class="d-flex flex-row h-50">
									<div class="w-50">
										<img src="img/${item.getImg_name()}" class="card-img-top"
											alt="item" style="height: 85%; width: 50%;">
									</div>
									<div class="w-50 d-flex flex-column">
										<div>
											<h5 class="card-title m-0 text-start">
												<c:out value="${item.getItem_name()}"></c:out>
											</h5>
										</div>
										<div>
											<p class="card-text mb-2">
												<c:out value="¥${item.getPrice()}"></c:out>
										</div>
										<div>
											<p class="card-text mb-4 fs-6">
												<c:out value="${item.getDetail()}"></c:out>
										</div>
										<div
											class="d-flex flex-row justify-content-around align-items-center mb-3">
											<div>
												<p class="card-text mb-0">
													<c:out value="Quantity : ${item.getTmpCnt()}"></c:out>
												</p>
											</div>
											<div class="w-25">
												<form method="POST" action="RemoveFromCart">
													<input type="hidden" name="item_id"
														value="${item.getItem_id()}" />
													<button type="submit"
														class="btn btn-outline-danger btn-circle btn-xl me-2">×</button>
												</form>
											</div>

										</div>
									</div>
								</div>
								<hr class="mt-1 mb-4">
							</c:forEach>
							<div class="d-flex flex-row justify-content-around">
								<div>
									<p>Total Price</p>
								</div>
								<div>
									<c:out value="¥${order_sum_price}"></c:out>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<form method="post" action="Purchase">
								<button type="submit" class="btn btn-outline-dark">Order
									Confirmation</button>
							</form>
							<button type="button" class="btn btn-outline-secondary"
								data-bs-dismiss="modal">Close</button>
						</div>
					</c:if>
				</div>
			</div>
		</div>

		<table>
			<c:if test="${empty(items)}">
				<div class="text-center mt-5">
					<p class="fs-1">NO ITEMS</p>
				</div>
			</c:if>
			<c:if test="${!empty(items)}">
				<div class="d-flex flex-row flex-wrap align-items-center">
					<c:forEach var="item" items="${items}">
						<div class="text-center mb-5 ms-4 h-50">
							<form method="POST" action="Move">
								<input type="hidden" name="item_id" value="${item.getItem_id()}" />
								<button type="submit" class="p-0 btn"
									style="color: black; border: none;">
									<div class="card" style="width: 18rem; height: 60%;">
										<img src="img/${item.getImg_name()}" class="card-img-top"
											alt="item" style="height: 250px;">
										<div class="card-body d-flex flex-column py-0">
											<div>
												<h5 class="card-title m-0">
													<c:out value="${item.getItem_name()}"></c:out>
												</h5>
												<p class="card-text mb-2">
													<c:out value="¥${item.getPrice()}"></c:out>
												</p>
											</div>
										</div>
									</div>
								</button>
							</form>
							<form method="POST" action="AddToCart" class="mt-2">
								<input type="hidden" name="item" value="${item.getItem_id()}">
								<button type="submit" class="btn btn-outline-dark">Add
									to cart</button>
							</form>
						</div>
					</c:forEach>
				</div>
			</c:if>
			<br>
		</table>
	</div>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery-3.7.0.min.js"></script>
</body>
</html>