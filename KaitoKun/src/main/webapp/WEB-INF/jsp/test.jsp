<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>Test Page</title>
</head>
<body>
	<header class="container-fluid  bg-primary">
		<div class="d-flex flex-row-reverse">
			<div class="col-1 my-2 text-center">
				<a href="#"></a>
			</div>
			<div class="col-1 my-2 text-center">
				<a href="#"></a>
			</div>
		</div>
	</header>

	<section>
		<div class="container py-5 px-5 h-100 w-50">
			<div
				class="row d-flex justify-content-center align-items-center h-100">
				<div class="col">
					<div class="card" id="list1"
						style="border-radius: .75rem; background-color: #eff1f2;">
						<div class="card-body py-4 px-4 px-md-5">
							<h1 class="text-center text-primary pb-3">Test Page</h1>
							<form method="POST" action="DB" style="margin: auto;"
								class="d-flex flex-column align-items-center mb-4">
								<input type="text" placeholder="ENTER YOUR NAME" name="name"
									class="w-50 mb-2" /> <input type="text"
									placeholder="ENTER PASSWORD" name="password" class="w-50 mb-2" />
								<button type="submit" class="btn btn-outline-primary px-5">Data</button>
							</form>
							<form method="GET" action="Create" style="margin: auto;"
								class="d-flex flex-column align-items-center">
								<button type="submit" class="btn btn-outline-warning px-5 fs-6">Create</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
</body>
</html>